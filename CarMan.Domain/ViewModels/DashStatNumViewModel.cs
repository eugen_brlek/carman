﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarMan.Domain.ViewModels
{
    public class DashStatNumViewModel
    {
        public DashStatNumViewModel()
        {

        }

        public int Employees { get; set; }
        public int Cars { get; set; }
        public int Travels { get; set; }
        public int CarTypes { get; set; }
    }
}
