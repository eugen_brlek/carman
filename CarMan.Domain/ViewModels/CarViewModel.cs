﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CarMan.Domain.ViewModels
{
    public class CarViewModel
    {
        public CarViewModel()
        {

        }

        public int Id { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Color { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Plates { get; set; }
        [Required]
        [Range(2, 7)]
        public int NumberOfSeats { get; set; }
        public DateTime DateCreated { get; set; }

        [Required]
        public int CarTypeId { get; set; }
        public CarTypeViewModel CarType { get; set; }

        public List<TravelViewModel> Travels { get; set; }     
    }
}
