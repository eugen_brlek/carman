﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CarMan.Domain.ViewModels
{
    public class EmployeeViewModel
    {
        public EmployeeViewModel()
        {

        }

        public int Id { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }
        [Required]
        public bool IsDriver { get; set; }
        public DateTime DateCreated { get; set; }

        public List<TravelEmployeeViewModel> TravelEmployees { get; set; }     
    }
}
