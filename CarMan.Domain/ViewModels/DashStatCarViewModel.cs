﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarMan.Domain.ViewModels
{
    public class DashStatCarViewModel
    {
        public DashStatCarViewModel()
        {

        }

        public string CarType { get; set; }
        public string CarName { get; set; }
        public int NumOfTravel { get; set; }
    }
}
