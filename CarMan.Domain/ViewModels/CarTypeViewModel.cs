﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CarMan.Domain.ViewModels
{
    public class CarTypeViewModel
    {
        public CarTypeViewModel()
        {

        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }

        public List<CarViewModel> Cars { get; set; }        
    }
}
