﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarMan.Domain.ViewModels
{
    public class TravelEmployeeViewModel
    {
        public TravelEmployeeViewModel()
        {
            //this.Travel = new TravelViewModel();
            //this.Employee = new EmployeeViewModel();
        }
        public TravelEmployeeViewModel(TravelEmployeeViewModel entity)
        {
            this.TravelId = entity.TravelId;
            this.EmployeeId = entity.EmployeeId;
        }

        public int TravelId { get; set; }
        public TravelViewModel Travel { get; set; }

        public int EmployeeId { get; set; }
        public EmployeeViewModel Employee { get; set; }         
    }
}
