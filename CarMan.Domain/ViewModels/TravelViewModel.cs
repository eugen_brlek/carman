﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarMan.Domain.ViewModels
{
    public class TravelViewModel
    {
        public TravelViewModel()
        {
            //this.Car = new CarViewModel();
            this.TravelEmployees = new List<TravelEmployeeViewModel>();
        }

        public int Id { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string StartLocation { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string EndLocation { get; set; }
        [Required]
        public DateTime? StartDate { get; set; }
        [Required]
        public DateTime? EndDate { get; set; }

        [Required]
        public int CarId { get; set; }
        public CarViewModel Car { get; set; }
        public DateTime DateCreated { get; set; }

        public List<TravelEmployeeViewModel> TravelEmployees { get; set; }      
    }
}
