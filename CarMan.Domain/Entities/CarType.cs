﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CarMan.Domain.Entities
{
    public class CarType : BaseEntity
    {
        public CarType()
        {

        }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }

        public ICollection<Car> Cars { get; set; }        
    }
}
