﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CarMan.Domain.Entities
{
    public class Employee : BaseEntity
    {
        public Employee()
        {

        }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }
        [Required]
        public bool IsDriver { get; set; }

        public ICollection<TravelEmployee> TravelEmployees { get; set; }    
    }
}
