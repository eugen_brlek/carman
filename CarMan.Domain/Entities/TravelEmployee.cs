﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CarMan.Domain.Entities
{
    public class TravelEmployee
    {
        public TravelEmployee()
        {

        }

        [Required]
        public int TravelId { get; set; }
        public Travel Travel { get; set; }

        [Required]
        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }      
    }
}
