﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarMan.Domain.Entities
{
    public class DashStatNum
    {
        public DashStatNum()
        {

        }
        public DashStatNum(int cars, int carTypes, int employees, int travels)
        {
            this.Employees = employees;
            this.Cars = cars;
            this.CarTypes = carTypes;
            this.Travels = travels;
        }

        public int Employees { get; set; }
        public int Cars { get; set; }
        public int Travels { get; set; }
        public int CarTypes { get; set; }
    }
}
