﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CarMan.Domain.Entities
{
    public class Travel : BaseEntity
    {
        public Travel()
        {

        }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string StartLocation { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string EndLocation { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }

        [Required]
        public int CarId { get; set; }
        public Car Car { get; set; }

        public ICollection<TravelEmployee> TravelEmployees { get; set; }       
    }
}
