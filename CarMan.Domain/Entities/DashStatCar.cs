﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarMan.Domain.Entities
{
    public class DashStatCar
    {
        public DashStatCar()
        {

        }

        public string CarType { get; set; }
        public string CarName { get; set; }
        public int NumOfTravel { get; set; }
    }
}
