﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarMan.Domain.Entities
{
    public class Car : BaseEntity
    {
        public Car()
        {

        }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }       
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Color { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Plates { get; set; }
        [Required]
        [Range(1, 7)]
        public int NumberOfSeats { get; set; }

        [Required]
        public int CarTypeId { get; set; }
        public CarType CarType { get; set; }

        public ICollection<Travel> Travels { get; set; }       
    }
}
