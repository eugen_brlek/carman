﻿namespace CarMan.Api.Helpers
{
    public class AppSettings
    {
        public string JwtSecurityKey { get; set; }
        public string JwtIssuer { get; set; }
        public string JwtExpiryInDays { get; set; }
    }
}
