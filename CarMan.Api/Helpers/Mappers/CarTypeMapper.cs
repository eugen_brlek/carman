﻿using AutoMapper;
using CarMan.Domain.Entities;
using CarMan.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Api.Helpers.Mappers
{
    public class CarTypeMapper : Profile
    {
        public CarTypeMapper()
        {
            CreateMap<CarType, CarTypeViewModel>();
            CreateMap<CarTypeViewModel, CarType>();
            CreateMap<CarTypeViewModel, IEnumerable<CarType>>();
            CreateMap<IEnumerable<CarType>, CarTypeViewModel>();
        }
    }
}
