﻿using AutoMapper;
using CarMan.Domain.Entities;
using CarMan.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Api.Helpers.Mappers
{
    public class DashStatNumMapper : Profile
    {
        public DashStatNumMapper()
        {
            CreateMap<DashStatNum, DashStatNumViewModel>();
            CreateMap<DashStatNumViewModel, DashStatNum>();
        }
    }
}
