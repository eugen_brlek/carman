﻿using AutoMapper;
using CarMan.Domain.Entities;
using CarMan.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Api.Helpers.Mappers
{
    public class EmployeeMapper : Profile
    {
        public EmployeeMapper()
        {
            CreateMap<Employee, EmployeeViewModel>();
            CreateMap<EmployeeViewModel, Employee>();
            CreateMap<EmployeeViewModel, IEnumerable<Employee>>();
            CreateMap<IEnumerable<Employee>, EmployeeViewModel>();
        }
    }
}
