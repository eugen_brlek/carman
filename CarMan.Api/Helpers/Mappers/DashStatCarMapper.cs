﻿using AutoMapper;
using CarMan.Domain.Entities;
using CarMan.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Api.Helpers.Mappers
{
    public class DashStatCarMapper : Profile
    {
        public DashStatCarMapper()
        {
            CreateMap<DashStatCar, DashStatCarViewModel>();
            CreateMap<DashStatCarViewModel, DashStatCar>();
            CreateMap<DashStatCarViewModel, IEnumerable<DashStatCar>>();
            CreateMap<IEnumerable<DashStatCar>, DashStatCarViewModel>();
        }
    }
}
