﻿using AutoMapper;
using CarMan.Domain.Entities;
using CarMan.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Api.Helpers.Mappers
{
    public class CarMapper : Profile
    {
        public CarMapper()
        {
            CreateMap<Car, CarViewModel>();
            CreateMap<CarViewModel, Car>();
            CreateMap<CarViewModel, IEnumerable<Car>>();
            CreateMap<IEnumerable<Car>, CarViewModel>();
        }
    }
}
