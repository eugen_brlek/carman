﻿using AutoMapper;
using CarMan.Domain.Entities;
using CarMan.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Api.Helpers.Mappers
{
    public class TravelEmployeeMapper : Profile
    {
        public TravelEmployeeMapper()
        {
            CreateMap<TravelEmployee, TravelEmployeeViewModel>();
            CreateMap<TravelEmployeeViewModel, TravelEmployee>();
            CreateMap<TravelEmployeeViewModel, IEnumerable<TravelEmployeeViewModel>>();
            CreateMap<IEnumerable<TravelEmployee>, TravelEmployeeViewModel>();
        }
    }
}
