﻿using AutoMapper;
using CarMan.Domain.Entities;
using CarMan.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Api.Helpers.Mappers
{
    public class TravelMapper : Profile
    {
        public TravelMapper()
        {
            CreateMap<Travel, TravelViewModel>();
            CreateMap<TravelViewModel, Travel>();
            CreateMap<TravelViewModel, IEnumerable<Travel>>();
            CreateMap<IEnumerable<Travel>, TravelViewModel>();
        }
    }
}
