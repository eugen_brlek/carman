﻿namespace CarMan.Api.Helpers
{
    public static class ApiConstants
    {
        public const string ERROR = "Error on submited request!!";
        public const string NULL = "Data is null!";
        public const string INVALID = "Invalid model data!";
        public const string NOT_FOUND = "Data not found!";
        public const string UNIQUE = "Unique error!";

        public const string DELETE_ERROR = "Delete action error!";
        public const string DELETE_SUCCESS = "Successfully deleted!";

        public const string UPDATE_ERROR = "Update action error!";
        public const string UPDATE_SUCCESS = "Data successfully updated!";

        public const string INSERT_ERROR = "Insert action error!";
        public const string INSERT_SUCCESS = "Data successfully inserted!";

        
    }
}
