﻿using CarMan.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CarMan.Api.Persistence
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        public DbSet<Car> Cars { get; set; }
        public DbSet<CarType> CarTypes { get; set; }
        public DbSet<Travel> Travels { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<TravelEmployee> TravelEmployees { get; set; }
        public DbSet<User> Users { get; set; }


        //public DbSet<DashStatCar> DashStatCars { get; set; }
        public DbQuery<DashStatCar> DashStatCars { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Configuring a many-to-many relationship
            modelBuilder.Entity<TravelEmployee>()
                .HasKey(x => new { x.TravelId, x.EmployeeId });
            modelBuilder.Entity<TravelEmployee>()
                .HasOne(x => x.Travel)
                .WithMany(y => y.TravelEmployees)
                .HasForeignKey(x => x.TravelId);
            modelBuilder.Entity<TravelEmployee>()
                .HasOne(x => x.Employee)
                .WithMany(y => y.TravelEmployees)
                .HasForeignKey(x => x.EmployeeId);

            // Configuring a unique atribute
            modelBuilder.Entity<Car>()
                .HasIndex(u => u.Plates)
                .IsUnique();
            modelBuilder.Entity<User>()
                .HasIndex(u => u.Email)
                .IsUnique();

            //Configuring a DB view
            //modelBuilder.Entity<DashStatCar>().HasNoKey();
            modelBuilder.Query<DashStatCar>().ToView("DashStatCar");
        }

        public async override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            var added = ChangeTracker.Entries<BaseEntity>().Where(E => E.State == EntityState.Added).ToList();

            added.ForEach(a =>
            {
                a.Property(p => p.DateCreated).CurrentValue = DateTime.UtcNow;
                a.Property(p => p.DateCreated).IsModified = true;
                a.Property(p => p.DateCreated).CurrentValue = DateTime.UtcNow;
                a.Property(p => p.DateCreated).IsModified = true;
            });           

            return await base.SaveChangesAsync(cancellationToken);
        }
    }
}
