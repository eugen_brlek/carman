﻿using CarMan.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarMan.Api.Services.Interfaces
{
    public interface ITravelEmployeeServices
    {
        Task<IEnumerable<TravelEmployee>> SelectAll();
        Task<TravelEmployee> SelectByTravelId(int? Id);
        Task<TravelEmployee> SelectByEmployeelId(int? Id);
        Task Create(TravelEmployee entity);
        Task Update(TravelEmployee entity, TravelEmployee newEntity);
        Task Delete(TravelEmployee entity);
    }
}
