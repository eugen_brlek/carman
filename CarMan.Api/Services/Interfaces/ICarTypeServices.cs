﻿using CarMan.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarMan.Api.Services.Interfaces
{
    public interface ICarTypeServices
    {
        Task<IEnumerable<CarType>> SelectAll();
        Task<CarType> SelectById(int? Id);
        Task Create(CarType entity);
        Task Update(CarType entity, CarType newEntity);
        Task Delete(CarType entity);
        Task<int> SelectCount();
    }
}
