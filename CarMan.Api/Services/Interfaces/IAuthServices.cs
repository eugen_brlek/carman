﻿
using CarMan.Domain.ViewModels;
using System.Threading.Tasks;

namespace CarMan.Api.Services.Interfaces
{
    public interface IAuthServices
    {
        Task<LoginViewModel> Authenticate(string email, string password);
    }
}
