﻿using CarMan.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarMan.Api.Services.Interfaces
{
    public interface ITravelServices
    {
        Task<IEnumerable<Travel>> SelectAll(DateTime? startDate, DateTime? endDate);
        Task<Travel> SelectById(int? Id);
        Task<bool> Create(Travel entity);
        Task<bool> Update(int? Id, Travel newEntity);
        Task Delete(Travel entity);
        Task<int> SelectCount();
    }
}
