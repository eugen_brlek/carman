﻿using CarMan.Domain.Entities;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarMan.Api.Services.Interfaces
{
    public interface IDashboardServices
    {
        Task<DashStatNum> GetSumStat();
        Task<IEnumerable<DashStatCar>> GetCarStat();
        Task<IEnumerable<DashStatNum>> GetEmpStat();
    }
}
