﻿using CarMan.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarMan.Api.Services.Interfaces
{
    public interface ICarServices
    {
        Task<IEnumerable<Car>> SelectAll();
        Task<IEnumerable<Car>> SelectAvailable(string StartLocation, string EndLocation, DateTime? StartDate, DateTime? EndDate);
        Task<Car> SelectById(int? Id);
        Task<bool> Create(Car entity);
        Task<bool> Update(int? Id, Car newEntity);
        Task Delete(Car entity);
        Task<int> SelectCount();
    }
}
