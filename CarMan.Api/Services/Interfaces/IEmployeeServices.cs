﻿using CarMan.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarMan.Api.Services.Interfaces
{
    public interface IEmployeeServices
    {
        Task<IEnumerable<Employee>> SelectAll(string empName);
        Task<Employee> SelectById(int? Id);
        Task Create(Employee entity);
        Task Update(Employee entity, Employee newEntity);
        Task Delete(Employee entity);
        Task<int> SelectCount();
    }
}
