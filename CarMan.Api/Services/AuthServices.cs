﻿using CarMan.Api.Helpers;
using CarMan.Api.Persistence;
using CarMan.Api.Services.Interfaces;
using CarMan.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CarMan.Api.Services
{
    public class AuthServices : IAuthServices
    {
        private readonly AppDbContext _context;
        private readonly AppSettings _appSettings;

        public AuthServices(
            AppDbContext appDbContext,
            IOptions<AppSettings> appSettings)
        {
            this._context = appDbContext;
            this._appSettings = appSettings.Value;
        }

        public async Task<LoginViewModel> Authenticate(string email, string password)
        {
            var user = await GetUserData(email, password);

            if (user == null)
            {
                return null;
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.JwtSecurityKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.Role)
                }),
                Expires = DateTime.UtcNow.AddHours(8),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);
            user.Password = null;

            return user;
        }

        private async Task<LoginViewModel> GetUserData(string email, string password)
        {
            var user = await _context.Users.SingleOrDefaultAsync(x => x.Email == email && x.Password == password);

            if (user == null)
            {
                return null;
            }

            LoginViewModel newLogin = new LoginViewModel();
            newLogin.Id = user.Id;
            newLogin.FirstName = user.FirstName;
            newLogin.LastName = user.LastName;
            newLogin.Email = user.Email;
            newLogin.Password = user.Password;
            newLogin.Role = user.Role;

            return newLogin;
        }
    }
}
