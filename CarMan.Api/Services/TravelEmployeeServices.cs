﻿using CarMan.Api.Persistence;
using CarMan.Api.Services.Interfaces;
using CarMan.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Api.Services
{
    public class TravelEmployeeServices : ITravelEmployeeServices
    {
        private readonly AppDbContext _context;

        public TravelEmployeeServices(
            AppDbContext appDbDontext)
        {
            this._context = appDbDontext;
        }

        public async Task Create(TravelEmployee entity)
        {
            if (entity != null)
            {
                await _context.TravelEmployees.AddAsync(entity);
                await _context.SaveChangesAsync();
            }
        }

        public async Task Delete(TravelEmployee entity)
        {
            if (entity != null)
            {
                _context.TravelEmployees.Remove(entity);
                await _context.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<TravelEmployee>> SelectAll()
        {
            return await _context.TravelEmployees
                                 .ToListAsync();
        }

        public async Task<TravelEmployee> SelectByTravelId(int? Id)
        {
            return await _context.TravelEmployees
                                 .Where(x => x.TravelId == Id)
                                 .FirstOrDefaultAsync();
        }

        public async Task<TravelEmployee> SelectByEmployeelId(int? Id)
        {
            return await _context.TravelEmployees
                                 .Where(x => x.EmployeeId == Id)
                                 .FirstOrDefaultAsync();
        }

        public async Task Update(TravelEmployee entity, TravelEmployee newEntity)
        {
            if (entity != null)
            {               
                entity.EmployeeId = newEntity.EmployeeId;
                entity.TravelId = newEntity.TravelId;

                _context.TravelEmployees.Update(entity).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
        }
    }
}
