﻿using CarMan.Api.Persistence;
using CarMan.Api.Services.Interfaces;
using CarMan.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Api.Services
{
    public class TravelServices : ITravelServices
    {
        private readonly AppDbContext _context;

        public TravelServices(
            AppDbContext appDbContext)
        {
            this._context = appDbContext;
        }

        public async Task<bool> Create(Travel entity)
        {
            if (entity != null)
            {
                var IsNumOfSeats = await CheckConstraintsNumOfSeats(entity);
                var IsDriverExist = await CheckConstraintsIsDriverExist(entity);

                if(IsDriverExist && IsNumOfSeats)
                {
                    await _context.Travels.AddAsync(entity);
                    await _context.SaveChangesAsync();
                    return true;
                }                                     
            }
            return false;
        }

        public async Task Delete(Travel entity)
        {
            if (entity != null)
            {
                _context.Travels.Remove(entity);
                await _context.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<Travel>> SelectAll(DateTime? startDate, DateTime? endDate)
        {
            if (startDate != null && endDate != null)
            {
                return await _context.Travels.Where(x => x.StartDate >= startDate && x.EndDate <= endDate)
                                             .OrderByDescending(x => x.DateCreated)
                                             .ToListAsync();
            }
            else if (startDate != null && endDate == null)
            {
                return await _context.Travels.Where(x => x.StartDate >= startDate)
                                             .OrderByDescending(x => x.DateCreated)
                                             .ToListAsync();
            }
            else if (startDate == null && endDate != null)
            {
                return await _context.Travels.Where(x => x.EndDate <= endDate)
                                             .OrderByDescending(x => x.DateCreated)
                                             .ToListAsync();
            }
            else
            {
                return await _context.Travels
                                     .OrderByDescending(x => x.DateCreated)
                                     .ToListAsync();
            }
        }

        public async Task<Travel> SelectById(int? Id)
        {
            return await _context.Travels
                                 .Include(x => x.TravelEmployees).ThenInclude(y => y.Employee)
                                 .Include(x => x.Car).ThenInclude(y => y.CarType)
                                 .Where(x => x.Id == Id)              
                                 .FirstOrDefaultAsync();
        }

        public async Task<bool> Update(int? Id, Travel entity)
        {

            var IsNumOfSeats = await CheckConstraintsNumOfSeats(entity);
            var IsDriverExist = await CheckConstraintsIsDriverExist(entity);

            if (IsDriverExist && IsNumOfSeats)
            {
                var existingParent = await _context.Travels
                                                .Where(x => x.Id == Id)
                                                .Include(x => x.TravelEmployees)
                                                .SingleOrDefaultAsync();

                if (existingParent != null)
                {
                    // Update parent
                    _context.Entry(existingParent).CurrentValues.SetValues(entity);
                }
                // Delete children
                foreach (var existingChild in existingParent.TravelEmployees.ToList())
                {
                    if (!entity.TravelEmployees.Any(c => c.EmployeeId == existingChild.EmployeeId && c.TravelId == existingChild.TravelId))
                        _context.TravelEmployees.Remove(existingChild);
                }

                // Update and Insert children
                foreach (var childModel in entity.TravelEmployees)
                {
                    var existingChild = existingParent.TravelEmployees
                                                      .Where(c => c.EmployeeId == childModel.EmployeeId && c.TravelId == childModel.TravelId)
                                                      .SingleOrDefault();

                    if (existingChild != null)
                        // Update child
                        _context.Entry(existingChild).CurrentValues.SetValues(childModel);
                    else
                    {
                        // Insert child
                        var newChild = new TravelEmployee
                        {
                            EmployeeId = childModel.EmployeeId,
                            TravelId = childModel.TravelId
                        };
                        existingParent.TravelEmployees.Add(newChild);
                    }
                }
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public async Task<int> SelectCount()
        {
            return await _context.Travels.CountAsync();
        }

        private async Task<bool> CheckConstraintsNumOfSeats(Travel entity)
        {       
            var numOfSeats = await _context.Cars
                                           .Where(x => x.Id == entity.CarId)
                                           .Select(x => x.NumberOfSeats)
                                           .SingleOrDefaultAsync();

            if (entity.TravelEmployees.Count > Convert.ToInt32(numOfSeats))
            {
                return false;
            }         
            return true;
        }

        private async Task<bool> CheckConstraintsIsDriverExist(Travel entity)
        {
            bool IsDriverExist = false;

            if (entity.TravelEmployees.Count > 0)
            {
                foreach (var employee in entity.TravelEmployees)
                {
                    if (await _context.Employees.Where(x => x.Id == employee.EmployeeId)
                                            .Where(x => x.IsDriver == true)
                                            .AnyAsync())
                    {
                        IsDriverExist = true;
                        break;
                    }
                }         
            }
            return IsDriverExist;
        }
    }
}
