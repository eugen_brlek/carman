﻿using CarMan.Api.Persistence;
using CarMan.Api.Services.Interfaces;
using CarMan.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Api.Services
{
    public class EmployeeServices : IEmployeeServices
    {
        private readonly AppDbContext _context;

        public EmployeeServices(
            AppDbContext appDbContext)
        {
            this._context = appDbContext;
        }

        public async Task Create(Employee entity)
        {
            if (entity != null)
            {
                await _context.Employees.AddAsync(entity);
                await _context.SaveChangesAsync();
            }
        }

        public async Task Delete(Employee entity)
        {
            if (entity != null)
            {
                _context.Employees.Remove(entity);
                await _context.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<Employee>> SelectAll(string empName)
        {
            if(empName != null)
            {
                return await _context.Employees
                                     .Where(x => x.Name.Contains(empName))
                                     .OrderByDescending(x => x.DateCreated)
                                     .ToListAsync();
            }
            else
            {
                return await _context.Employees
                                     .OrderByDescending(x => x.DateCreated)
                                     .ToListAsync();
            }
        }

        public async Task<Employee> SelectById(int? Id)
        {
            return await _context.Employees.Where(x => x.Id == Id)
                                           .FirstOrDefaultAsync();
        }

        public async Task Update(Employee entity, Employee newEntity)
        {
            if (entity != null)
            {
                entity.Name = newEntity.Name;
                entity.IsDriver = newEntity.IsDriver;

                _context.Employees.Update(entity).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
        }

        public async Task<int> SelectCount()
        {
            return await _context.Employees.CountAsync();
        }
    }
}
