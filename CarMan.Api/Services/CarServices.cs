﻿using CarMan.Api.Persistence;
using CarMan.Api.Services.Interfaces;
using CarMan.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Api.Services
{
    public class CarServices : ICarServices
    {
        private readonly AppDbContext _context;

        public CarServices(
            AppDbContext appDbContext)
        {
            this._context = appDbContext;
        }

        public async Task<bool> Create(Car entity)
        {
            if (entity != null)
            {               
                var exist = _context.Cars.Count(x => x.Plates == entity.Plates);

                if(exist == 0)
                {                  
                    await _context.Cars.AddAsync(entity);
                    await _context.SaveChangesAsync();
                    return true;
                }             
            }
            return false;
        }

        public async Task Delete(Car entity)
        {
            if (entity != null)
            {
                _context.Cars.Remove(entity);
                await _context.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<Car>> SelectAll()
        {
            return await _context.Cars
                                 .Include(x => x.CarType)
                                 .OrderByDescending(x => x.DateCreated)
                                 .ToListAsync();
        }

        public async Task<IEnumerable<Car>> SelectAvailable(string StartLocation, string EndLocation, DateTime? StartDate, DateTime? EndDate)
        {
            if ( StartDate > EndDate )
            {
                //return await Task.FromResult<IEnumerable<Car>>(null);
                throw new Exception("StartDate > EndDate");
            }

            IList<Car> allCars = await _context.Cars
                                               .Include(x => x.CarType)
                                               .ToListAsync();

            IList<Car> notAvailable = await _context.Travels
                                                    .Include(x => x.Car)
                                                    .Where(x =>  !(StartDate < x.StartDate && EndDate < x.StartDate || StartDate >  x.EndDate && EndDate > x.EndDate))
                                                    .Where(x => x.EndLocation != StartLocation)
                                                    .Select(x => x.Car)
                                                    .Include(x => x.CarType)
                                                    .ToListAsync();

            var availableCars = allCars.Except(notAvailable).ToList();

            return availableCars;
        }

        public async Task<Car> SelectById(int? Id)
        {
            return await _context.Cars
                                 .Include(x => x.CarType)
                                 .Where(x => x.Id == Id)
                                 .FirstOrDefaultAsync();
        }

        public async Task<bool> Update(int? Id, Car newEntity)
        {
            if (newEntity != null)
            {
                var entityToUpdate = await _context.Cars.FirstOrDefaultAsync(x => x.Id == Id);

                var unique = await _context.Cars.AnyAsync(x => x.Plates == newEntity.Plates && x.Id != newEntity.Id);

                if (!unique)
                {
                    entityToUpdate.Name = newEntity.Name;
                    entityToUpdate.Plates = newEntity.Plates;
                    entityToUpdate.NumberOfSeats = newEntity.NumberOfSeats;
                    entityToUpdate.Color = newEntity.Color;
                    entityToUpdate.CarTypeId = newEntity.CarTypeId;

                    _context.Cars.Update(entityToUpdate).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                    return true;
                }               
            }
            return false;
        }

        public async Task<int> SelectCount()
        {
            return await _context.Cars.CountAsync();
        }      
    }
}
