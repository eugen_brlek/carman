﻿using CarMan.Api.Persistence;
using CarMan.Api.Services.Interfaces;
using CarMan.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Api.Services
{
    public class DashboardServices : IDashboardServices
    {
        private readonly AppDbContext _context;
        private readonly IEmployeeServices _employeeServices;
        private readonly ICarServices _carServices;
        private readonly ICarTypeServices _carTypeServices;
        private readonly ITravelServices _travelServices;

        public DashboardServices(
            AppDbContext appDbContext,
            ICarServices carServices,
            ICarTypeServices carTypeServices,
            IEmployeeServices employeeServices,
            ITravelServices travelServices
            )
        {
            this._context = appDbContext;
            this._employeeServices = employeeServices;
            this._carServices = carServices;
            this._carTypeServices = carTypeServices;
            this._travelServices = travelServices;
        }       

        public async Task<DashStatNum> GetSumStat()
        {
            var carSum = await _carServices.SelectCount();
            var carTypeSum = await _carTypeServices.SelectCount();
            var employeeSum = await _employeeServices.SelectCount();
            var travelSum = await _travelServices.SelectCount();

            DashStatNum dashStatNum = new DashStatNum(carSum, carTypeSum, employeeSum, travelSum);

            return dashStatNum;
        }

        public async Task<IEnumerable<DashStatCar>> GetCarStat()
        {
            return await _context.DashStatCars
                                 .AsNoTracking()
                                 .ToListAsync();

        }

        public async Task<IEnumerable<DashStatNum>> GetEmpStat()
        {
            throw new NotImplementedException();
        }
    }
}
