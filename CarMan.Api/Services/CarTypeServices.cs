﻿using CarMan.Api.Persistence;
using CarMan.Api.Services.Interfaces;
using CarMan.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Api.Services
{
    public class CarTypeServices : ICarTypeServices
    {
        private readonly AppDbContext _context;

        public CarTypeServices(
            AppDbContext appDbContext)
        {
            this._context = appDbContext;
        }

        public async Task Create(CarType entity)
        {
            if (entity != null)
            {
                await _context.CarTypes.AddAsync(entity);
                await _context.SaveChangesAsync();
            }
        }

        public async Task Delete(CarType entity)
        {
            if (entity != null)
            {
                _context.CarTypes.Remove(entity);
                await _context.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<CarType>> SelectAll()
        {
            return await _context.CarTypes
                                 .OrderByDescending(x => x.DateCreated)
                                 .ToListAsync();
        }

        public async Task<CarType> SelectById(int? Id)
        {
            return await _context.CarTypes
                                 .Where(x => x.Id == Id)
                                 .FirstOrDefaultAsync();
        }

        public async Task Update(CarType entity, CarType newEntity)
        {
            if (entity != null)
            {
                entity.Name = newEntity.Name;                

                _context.CarTypes.Update(entity).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
        }

        public async Task<int> SelectCount()
        {
            return await _context.CarTypes.CountAsync();
        }
    }
}
