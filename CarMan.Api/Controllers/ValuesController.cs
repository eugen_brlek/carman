﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CarMan.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "CarMan, ver. 1.2" };
        }
    }
}