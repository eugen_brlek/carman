﻿using AutoMapper;
using CarMan.Api.Helpers;
using CarMan.Api.Services.Interfaces;
using CarMan.Domain.Entities;
using CarMan.Domain.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TravelController : ControllerBase
    {
        private readonly ITravelServices _services;
        private readonly IMapper _mapper;

        public TravelController(
            ITravelServices travelServices,
            IMapper mapper)
        {
            this._services = travelServices;
            this._mapper = mapper;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAll(DateTime? startDate, DateTime? endDate)
        {
            try
            {
                IEnumerable<Travel> travels = await _services.SelectAll(startDate, endDate);              
           
                var resultToReturn = _mapper.Map<IEnumerable<TravelViewModel>>(travels);
                             
                return Ok(resultToReturn);
            }
            catch (Exception)
            {
                return StatusCode(404, new { message = ApiConstants.NOT_FOUND });
            }
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int? id)
        {
            if (id == null)
            {
                return BadRequest(new { message = ApiConstants.NULL, id });
            }

            try
            {
                var travel = await _services.SelectById(id);

                var resultToReturn = _mapper.Map<TravelViewModel>(travel);

                if (resultToReturn == null)
                {
                    return StatusCode(404, new { message = ApiConstants.NOT_FOUND, id });
                }

                return Ok(resultToReturn);
            }
            catch (Exception)
            {
                return StatusCode(404, new { message = ApiConstants.NOT_FOUND });
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] TravelViewModel entity)
        {
            if (entity == null)
            {
                return BadRequest(new { message = ApiConstants.NULL });
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(new { message = ApiConstants.INVALID });
            }

            try
            {
                var travel = _mapper.Map<Travel>(entity);

                bool result = await _services.Create(travel);

                if (!result)
                {
                    return StatusCode(412, new { message = ApiConstants.INVALID });
                }

                return Ok(new { message = ApiConstants.INSERT_SUCCESS, travel.Id });
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ApiConstants.INSERT_ERROR, ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<IActionResult> Update([FromBody] TravelViewModel entity, int? id)
        {
            if (entity == null || id == null)
            {
                return BadRequest(new { message = ApiConstants.NULL });
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(new { message = ApiConstants.INVALID });
            }

            try
            {
                var newTravel = _mapper.Map<Travel>(entity);

                bool result = await _services.Update(id, newTravel);

                if (!result)
                {
                    return StatusCode(412, new { message = ApiConstants.INVALID });
                }

                return Ok(new { message = ApiConstants.UPDATE_SUCCESS });
            }
            catch (Exception)
            {
                return BadRequest(new { message = ApiConstants.UPDATE_ERROR });
            }
        }

        [AllowAnonymous]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return BadRequest(new { message = ApiConstants.NULL });
            }

            try
            {
                var travel = await _services.SelectById(id);

                if (travel == null)
                {
                    return StatusCode(404, new { message = ApiConstants.NOT_FOUND, id });
                }

                await _services.Delete(travel);

                return Ok(new { message = ApiConstants.DELETE_SUCCESS });
            }
            catch (Exception)
            {
                return BadRequest(new { message = ApiConstants.DELETE_ERROR });
            }
        }
    }
}