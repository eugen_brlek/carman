﻿using AutoMapper;
using CarMan.Api.Helpers;
using CarMan.Api.Services.Interfaces;
using CarMan.Domain.Entities;
using CarMan.Domain.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarMan.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CarController : ControllerBase
    {
        private readonly ICarServices _services;
        private readonly IMapper _mapper;

        public CarController(
            ICarServices carService,
            IMapper mapper)
        {
            this._services = carService;
            this._mapper = mapper;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                IEnumerable<Car> cars = await _services.SelectAll();

                var resultToReturn = _mapper.Map<IEnumerable<CarViewModel>>(cars);

                return Ok(resultToReturn);
            }
            catch (Exception)
            {
                return StatusCode(404, new { message = ApiConstants.NOT_FOUND });
            }
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int? id)
        {
            if (id == null)
            {
                return BadRequest(new { message = ApiConstants.NULL, id });
            }

            try
            {
                var car = await _services.SelectById(id);

                var resultToReturn = _mapper.Map<CarViewModel>(car);

                if (resultToReturn == null)
                {
                    return StatusCode(404, new { message = ApiConstants.NOT_FOUND, id });
                }

                return Ok(resultToReturn);
            }
            catch (Exception)
            {
                return StatusCode(404, new { message = ApiConstants.NOT_FOUND });
            }
        }

        [AllowAnonymous]
        [HttpGet("GetAvailable")]
        public async Task<IActionResult> GetAvailable(string StartLocation, string EndLocation, DateTime? StartDate, DateTime? EndDate)
        {
            try
            {
                IEnumerable<Car> cars = await _services.SelectAvailable(StartLocation, EndLocation, StartDate, EndDate);

                var resultToReturn = _mapper.Map<IEnumerable<CarViewModel>>(cars);

                if(resultToReturn == null)
                {
                    return StatusCode(404, new { message = ApiConstants.NOT_FOUND });
                }

                return Ok(resultToReturn);
            }
            catch (Exception ex)
            {
                return StatusCode(404, new { /*message = ApiConstants.NOT_FOUND,*/ ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] CarViewModel entity)
        {
            if (entity == null)
            {
                return BadRequest(new { message = ApiConstants.NULL });
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(new { message = ApiConstants.INVALID });
            }

            try
            {
                var car = _mapper.Map<Car>(entity);

                var result = await _services.Create(car);

                if (!result)
                {
                    return StatusCode(409, new { message = ApiConstants.UNIQUE });
                }

                return Ok(new { message = ApiConstants.INSERT_SUCCESS, car });
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ApiConstants.INSERT_ERROR, ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<IActionResult> Update([FromBody] CarViewModel entity, int? id)
        {
            if (entity == null || id == null)
            {
                return BadRequest(new { message = ApiConstants.NULL });
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(new { message = ApiConstants.INVALID });
            }

            try
            {

                var car = _mapper.Map<Car>(entity);

                bool result = await _services.Update(id, car);

                if (!result)
                {
                    //return StatusCode(409, new { message = ApiConstants.UNIQUE });
                    return BadRequest(new { message = ApiConstants.UNIQUE });
                }

                return Ok(new { message = ApiConstants.UPDATE_SUCCESS });
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ApiConstants.UPDATE_ERROR, ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return BadRequest(new { message = ApiConstants.NULL });
            }

            try
            {
                var car = await _services.SelectById(id);

                if (car == null)
                {
                    return NotFound(new { message = ApiConstants.NOT_FOUND, id });
                }

                await _services.Delete(car);

                return Ok(new { message = ApiConstants.DELETE_SUCCESS });
            }
            catch (Exception)
            {
                return BadRequest(new { message = ApiConstants.DELETE_ERROR });
            }
        }
    }
}