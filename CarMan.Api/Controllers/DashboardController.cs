﻿using AutoMapper;
using CarMan.Api.Helpers;
using CarMan.Api.Services.Interfaces;
using CarMan.Domain.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarMan.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        private readonly IDashboardServices _services;
        private readonly IMapper _mapper;

        public DashboardController(
            IDashboardServices dashboardServices,
            IMapper mapper)
        {
            this._services = dashboardServices;
            this._mapper = mapper;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetSumStat()
        {
            try
            {
                var dashboard = await _services.GetSumStat();

                var resultToReturn = _mapper.Map<DashStatNumViewModel>(dashboard);

                return Ok(resultToReturn);
            }
            catch (Exception)
            {
                return StatusCode(404, new { message = ApiConstants.NOT_FOUND });
            }
        }

        [AllowAnonymous]
        [HttpGet("GetCarStat")]
        public async Task<IActionResult> GetCarStat()
        {
            try
            {
                var dashboard = await _services.GetCarStat();

                var resultToReturn = _mapper.Map<IEnumerable<DashStatCarViewModel>>(dashboard);

                return Ok(dashboard);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return StatusCode(404, new { message = ApiConstants.NOT_FOUND });
            }
        }
    }
}