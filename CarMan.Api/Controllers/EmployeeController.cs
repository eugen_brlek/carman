﻿using AutoMapper;
using CarMan.Api.Helpers;
using CarMan.Api.Services.Interfaces;
using CarMan.Domain.Entities;
using CarMan.Domain.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarMan.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeServices _services;
        private readonly IMapper _mapper;

        public EmployeeController(
            IEmployeeServices employeeServices,
            IMapper mapper)
        {
            this._services = employeeServices;
            this._mapper = mapper;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery]string empName)
        {
            try
            {
                IEnumerable<Employee> employees = await _services.SelectAll(empName);

                var resultToReturn = _mapper.Map<IEnumerable<EmployeeViewModel>>(employees);

                return Ok(resultToReturn);
            }
            catch (Exception)
            {
                return StatusCode(404, new { message = ApiConstants.NOT_FOUND });
            }
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int? id)
        {
            if (id == null)
            {
                return BadRequest(new { message = ApiConstants.NULL, id });
            }

            try
            {
                var employee = await _services.SelectById(id);

                var resultToReturn = _mapper.Map<EmployeeViewModel>(employee);

                if (resultToReturn == null)
                {
                    return StatusCode(404, new { message = ApiConstants.NOT_FOUND, id });
                }

                return Ok(resultToReturn);
            }
            catch (Exception)
            {
                return StatusCode(404, new { message = ApiConstants.NOT_FOUND });
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] EmployeeViewModel employeeViewModel)
        {
            if (employeeViewModel == null)
            {
                return BadRequest(new { message = ApiConstants.NULL });
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(new { message = ApiConstants.INVALID });
            }

            try
            {
                var employee = _mapper.Map<Employee>(employeeViewModel);

                await _services.Create(employee);

                return Ok(new { message = ApiConstants.INSERT_SUCCESS, employee.Id });
            }
            catch (Exception)
            {
                return BadRequest(new { message = ApiConstants.INSERT_ERROR });
            }
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<IActionResult> Update([FromBody] EmployeeViewModel employeeViewModel, int? id)
        {
            if (employeeViewModel == null || id == null)
            {
                return BadRequest(new { message = ApiConstants.NULL });
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(new { message = ApiConstants.INVALID });
            }

            try
            {
                var employee = await _services.SelectById(id);

                var newEmployee = _mapper.Map<Employee>(employeeViewModel);

                await _services.Update(employee, newEmployee);

                return Ok(new { message = ApiConstants.UPDATE_SUCCESS });
            }
            catch (Exception)
            {
                return BadRequest(new { message = ApiConstants.UPDATE_ERROR });
            }
        }

        [AllowAnonymous]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return BadRequest(new { message = ApiConstants.NULL });
            }

            try
            {
                var employee = await _services.SelectById(id);

                if (employee == null)
                {
                    return StatusCode(404, new { message = ApiConstants.NOT_FOUND, id });
                }

                await _services.Delete(employee);

                return Ok(new { message = ApiConstants.DELETE_SUCCESS });
            }
            catch (Exception)
            {
                return BadRequest(new { message = ApiConstants.DELETE_ERROR });
            }
        }
    }
}