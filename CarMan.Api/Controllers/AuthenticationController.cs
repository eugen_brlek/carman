﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarMan.Api.Helpers;
using CarMan.Api.Services.Interfaces;
using CarMan.Domain.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CarMan.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthServices _services;

        public AuthenticationController(
            IAuthServices authServices)
        {
            this._services = authServices;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Authenticate([FromBody] LoginViewModel login)
        {
            try
            {
                var user = await _services.Authenticate(login.Email, login.Password);

                if (user == null)
                {
                    return StatusCode(404, new { message = ApiConstants.NOT_FOUND });
                }

                return Ok(user);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { message = ex.Message });
            }
        }
    }
}