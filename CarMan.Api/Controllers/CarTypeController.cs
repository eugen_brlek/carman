﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CarMan.Api.Helpers;
using CarMan.Api.Services.Interfaces;
using CarMan.Domain.Entities;
using CarMan.Domain.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CarMan.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CarTypeController : ControllerBase
    {
        private readonly ICarTypeServices _services;
        private readonly IMapper _mapper;

        public CarTypeController(
            ICarTypeServices carTypeServices,
            IMapper mapper)
        {
            this._services = carTypeServices;
            this._mapper = mapper;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                IEnumerable<CarType> carType = await _services.SelectAll();

                var resultToReturn = _mapper.Map<IEnumerable<CarTypeViewModel>>(carType);

                return Ok(resultToReturn);
            }
            catch (Exception)
            {
                return StatusCode(404, new { message = ApiConstants.NOT_FOUND });
            }
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int? Id)
        {
            if (Id == null)
            {
                return BadRequest(new { message = ApiConstants.NULL, Id });
            }

            try
            {
                var carType = await _services.SelectById(Id);

                var resultToReturn = _mapper.Map<CarTypeViewModel>(carType);

                if (resultToReturn == null)
                {
                    return StatusCode(404, new { message = ApiConstants.NOT_FOUND, Id });
                }

                return Ok(resultToReturn);
            }
            catch (Exception)
            {
                return StatusCode(404, new { message = ApiConstants.NOT_FOUND });
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] CarTypeViewModel entity)
        {
            if (entity == null)
            {
                return BadRequest(new { message = ApiConstants.NULL });
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(new { message = ApiConstants.INVALID });
            }

            try
            {
                var carType = _mapper.Map<CarType>(entity);

                await _services.Create(carType);

                return Ok(new { message = ApiConstants.INSERT_SUCCESS, carType });
            }
            catch (Exception)
            {
                return BadRequest(new { message = ApiConstants.INSERT_ERROR });
            }
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<IActionResult> Update([FromBody] CarTypeViewModel entity, int? id)
        {
            if (entity == null || id == null)
            {
                return BadRequest(new { message = ApiConstants.NULL });
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(new { message = ApiConstants.INVALID });
            }

            try
            {
                var oldCarType = await _services.SelectById(id);

                var newCarType = _mapper.Map<CarType>(entity);

                await _services.Update(oldCarType, newCarType);

                return Ok(new { message = ApiConstants.UPDATE_SUCCESS });
            }
            catch (Exception)
            {
                return BadRequest(new { message = ApiConstants.UPDATE_ERROR });
            }
        }

        [AllowAnonymous]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return BadRequest(new { message = ApiConstants.NULL });
            }

            try
            {
                var carType = await _services.SelectById(id);

                if (carType == null)
                {
                    return StatusCode(404, new { message = ApiConstants.NOT_FOUND, id });
                }

                await _services.Delete(carType);

                return Ok(new { message = ApiConstants.DELETE_SUCCESS });
            }
            catch (Exception)
            {
                return BadRequest(new { message = ApiConstants.DELETE_ERROR });
            }
        }
    }
}