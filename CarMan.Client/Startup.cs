using Blazor.Extensions.Storage;
using CarMan.Client.Services;
using Microsoft.AspNetCore.Components.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Sotsera.Blazor.Toaster.Core.Models;
using Toolbelt.Blazor.Extensions.DependencyInjection;

namespace CarMan.Client
{
    public class Startup
    {      
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<CarServices>();
            services.AddSingleton<CarTypeServices>();
            services.AddSingleton<EmployeeServices>();
            services.AddSingleton<TravelServices>();
            services.AddSingleton<AuthServices>();
            services.AddSingleton<DashboardServices>();

            services.AddStorage();
            services.AddLoadingBar();
            services.AddToaster(config =>
            {
                //example customizations
                config.PositionClass = Defaults.Classes.Position.TopRight;
                config.PreventDuplicates = true;
                config.NewestOnTop = false;
            });
        }

        public void Configure(IComponentsApplicationBuilder app)
        {
            app.AddComponent<App>("app");
            app.UseLoadingBar();
        }
    }
}
