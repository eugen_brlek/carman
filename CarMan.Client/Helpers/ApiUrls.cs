﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Client.Helpers
{
    public static class ApiUrls
    {
        public const string Login = "https://localhost:44327/api/Authentication";     
        public const string Employee = "https://localhost:44327/api/Employee";
        public const string Car = "https://localhost:44327/api/Car";
        public const string CarType = "https://localhost:44327/api/CarType";
        public const string Travel = "https://localhost:44327/api/Travel";
        public const string TravelEmployee = "https://localhost:44327/api/TravelEmployee";
        public const string Dashboard = "https://localhost:44327/api/dashboard";
        public const string DashboardCar = "https://localhost:44327/api/dashboard/GetCarStat";
    }
}
