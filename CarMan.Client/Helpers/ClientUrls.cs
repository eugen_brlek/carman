﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Client.Helpers
{
    public static class ClientUrls
    {
        public const string Employee = "/employee";
        public const string EmployeeAdd = "/employee/add";
        public const string EmployeeEdit = "/employee/edit/";
        public const string EmployeeDetails = "/employee/details/";

        public const string Car = "/car";
        public const string CarAdd = "/car/add";
        public const string CarEdit = "/car/edit/";
        public const string CarDetails = "/car/details/";

        public const string CarType = "/carType";
        public const string CarTypeAdd = "/carType/add";
        public const string CarTypeEdit = "/carType/edit/";
        public const string CarTypeDetails = "/carType/details/";

        public const string Travel = "/travel";
        public const string TravelAdd = "/travel/add";
        public const string TravelEdit = "/travel/edit/";
        public const string TravelDetails = "/travel/details/";
    }
}
