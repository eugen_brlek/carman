﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Client.Helpers
{
    public class JwtToken
    {
        public string Token { get; set; }
        public DateTime TimeExpire { get; set; }
    }
}
