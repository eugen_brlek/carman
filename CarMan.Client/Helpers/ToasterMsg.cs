﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Client.Helpers
{
    public static class ToasterMsg
    {
        public const string Success = "Transaction successfully completed!";
        public const string Warning = "Transaction canceled!";
        public const string Error = "Transaction unsuccessful!";
        public const string Info = "Transaction info!";
    }
}
