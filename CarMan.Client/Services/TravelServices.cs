﻿using CarMan.Client.Helpers;
using CarMan.Client.Services.Interfaces;
using CarMan.Domain.ViewModels;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CarMan.Client.Services
{
    public class TravelServices : ITravelServices
    {
        private readonly HttpClient _httpClient;
        private const string URL = ApiUrls.Travel;
        public bool IsCreated { get; private set; }
        public bool IsValid { get; private set; }
        public bool IsDeleted { get; private set; }
        public bool IsUpdated { get; private set; }
        public bool InTransaction { get; private set; }

        public TravelServices(
            HttpClient httpClient)
        {
            this._httpClient = httpClient;
        }

        public async Task Create(TravelViewModel entity)
        {
            var response = await _httpClient.PostAsync(URL, new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, "application/json"));

            if (response.IsSuccessStatusCode)
            {
                IsCreated = true;
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.Conflict)
            {
                IsValid = false;
            }
            else
            {
                IsCreated = false;
            }
        }

        public async Task Delete(int? Id)
        {
            var reponse = await _httpClient.DeleteAsync($"{URL}/{Id}");

            if (reponse.IsSuccessStatusCode)
            {
                IsDeleted = true;
            }
            else
            {
                IsDeleted = false;
            }
        }

        public async Task<List<TravelViewModel>> SelectAll()
        {
            return await _httpClient.GetJsonAsync<List<TravelViewModel>>(URL);
        }

        public async Task<TravelViewModel> SelectById(int? Id)
        {
            return await _httpClient.GetJsonAsync<TravelViewModel>($"{URL}/{Id}");
        }

        public async Task Update(TravelViewModel entity, int? Id)
        {
            var response = await _httpClient.PutAsync($"{URL}/{Id}", new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, "application/json"));

            if (response.IsSuccessStatusCode)
            {
                IsUpdated = true;
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.Conflict)
            {
                IsValid = false;
            }
            else
            {
                IsUpdated = false;
            }
        }

        public async Task<List<TravelViewModel>> SelectAllSearched(DateTime? StartDate, DateTime? EndDate)
        {
            string  par = "?startDate=" + StartDate + "&endDate=" + EndDate;

            return await _httpClient.GetJsonAsync<List<TravelViewModel>>(URL + par);
        }
    }
}
