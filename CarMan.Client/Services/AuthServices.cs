﻿using Blazor.Extensions.Storage;
using CarMan.Client.Helpers;
using CarMan.Client.Services.Interfaces;
using CarMan.Domain.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using static CarMan.Client.Helpers.JwtToken;

namespace CarMan.Client.Services
{
    public class AuthServices : IAuthServices
    {
        private readonly HttpClient _httpClient;
        private readonly LocalStorage _localStorage;

        public bool IsLoggedIn { get; private set; }
        private const string URL = ApiUrls.Login;
        private const string AuthTokenName = "authToken";

        public AuthServices(
            HttpClient httpClient,
            LocalStorage localStorage)
        {
            this._httpClient = httpClient;
            this._localStorage = localStorage;
        }

        public async Task Login(LoginViewModel entity)
        {

            var response = await _httpClient.PostAsync(URL, new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, "application/json"));

            if (response.IsSuccessStatusCode)
            {
                await SaveToken(response);
                await SetAuthorizationHeader();
                IsLoggedIn = true;
            }
            else if(response.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                IsLoggedIn = false;
            }
        }

        public async Task LogOut()
        {
            await _localStorage.RemoveItem(AuthTokenName);
            RemoveAuthorizationHeader();

            IsLoggedIn = false;
        }

        public async Task LogCheck()
        {
            var status = await _localStorage.GetItem<string>(AuthTokenName);

            if (status != null)
            {
                await SetAuthorizationHeader();
                IsLoggedIn = true;
            }
            else
            {
                await _localStorage.RemoveItem(AuthTokenName);
                RemoveAuthorizationHeader();
                IsLoggedIn = false;
            }
        }

        private async Task SaveToken(HttpResponseMessage response)
        {
            var responseContent = await response.Content.ReadAsStringAsync();
            var jwt = JsonConvert.DeserializeObject<JwtToken>(responseContent);

            await _localStorage.SetItem(AuthTokenName, jwt.Token);
        }

        private async Task SetAuthorizationHeader()
        {
            if (!_httpClient.DefaultRequestHeaders.Contains("Authorization"))
            {
                var token = await _localStorage.GetItem<string>(AuthTokenName);
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
        }

        private void RemoveAuthorizationHeader()
        {
            if (_httpClient.DefaultRequestHeaders.Contains("Authorization"))
            {
                _httpClient.DefaultRequestHeaders.Remove("Authorization");
            }
        }
    }
}
