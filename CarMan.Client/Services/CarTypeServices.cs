﻿using CarMan.Client.Helpers;
using CarMan.Client.Services.Interfaces;
using CarMan.Domain.ViewModels;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CarMan.Client.Services
{
    public class CarTypeServices : ICarTypeServices
    {
        private readonly HttpClient _httpClient;
        private const string URL = ApiUrls.CarType;
        public bool IsCreated { get; private set; }
        public bool IsValid { get; private set; }
        public bool IsDeleted { get; private set; }
        public bool IsUpdated { get; private set; }

        public CarTypeServices(
            HttpClient httpClient)
        {
            this._httpClient = httpClient;
        }

        public async Task<List<CarTypeViewModel>> SelectAll()
        {
            return await _httpClient.GetJsonAsync<List<CarTypeViewModel>>(URL);
        }

        public async Task<CarTypeViewModel> SelectById(int? Id)
        {
            return await _httpClient.GetJsonAsync<CarTypeViewModel>($"{URL}/{Id}");
        }

        public async Task Create(CarTypeViewModel entity)
        {
            var response = await _httpClient.PostAsync(URL, new StringContent(JsonSerializer.Serialize(entity), Encoding.UTF8, "application/json"));

            if (response.IsSuccessStatusCode)
            {
                IsCreated = true;
            }
            else
            {
                IsCreated = false;
            }
        }

        public async Task Update(CarTypeViewModel entity, int? Id)
        {
            var reponse = await _httpClient.PutAsync($"{URL}/{Id}", new StringContent(JsonSerializer.Serialize(entity), Encoding.UTF8, "application/json"));

            if (reponse.IsSuccessStatusCode)
            {
                IsUpdated = true;
            }
            else
            {
                IsUpdated = false;
            }
        }

        public async Task Delete(int? Id)
        {
            var reponse = await _httpClient.DeleteAsync($"{URL}/{Id}");

            if (reponse.IsSuccessStatusCode)
            {
                IsDeleted = true;
            }
            else
            {
                IsDeleted = false;
            }
        }
    }
}
