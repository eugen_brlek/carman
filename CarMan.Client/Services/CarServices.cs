﻿using CarMan.Client.Helpers;
using CarMan.Client.Services.Interfaces;
using CarMan.Domain.ViewModels;
using Microsoft.AspNetCore.Blazor.Http;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CarMan.Client.Services
{
    public class CarServices : ICarServices
    {
        private readonly HttpClient _httpClient;
        private const string URL = ApiUrls.Car;
        public bool IsCreated { get; private set; }
        public bool IsValid { get; private set; }
        public bool IsDeleted { get; private set; }
        public bool IsUpdated { get; private set; }

        public CarServices(
            HttpClient httpClient)
        {
            this._httpClient = httpClient;
        }

        public async Task Create(CarViewModel entity)
        {
            var response = await _httpClient.PostAsync(URL, new StringContent(JsonSerializer.Serialize(entity), Encoding.UTF8, "application/json"));

            if (response.IsSuccessStatusCode)
            {
                IsCreated = true;
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.Conflict)
            {
                IsValid = false;
            }
            else
            {
                IsCreated = false;
            }
        }

        public async Task Delete(int? Id)
        {
            var response = await _httpClient.DeleteAsync($"{URL}/{Id}");

            if (response.IsSuccessStatusCode)
            {
                IsDeleted = true;
            }
            else
            {
                IsDeleted = false;
            }
        }

        public async Task<List<CarViewModel>> SelectAll()
        {
            return await _httpClient.GetJsonAsync<List<CarViewModel>>(URL);
        }

        public async Task<List<CarViewModel>> SelectAvailable(string StartLocation, string EndLocation, DateTime? StartDate, DateTime? EndDate)
        {
            string par = "/GetAvailable?StartLocation=" + StartLocation + "&EndLocation=" + EndLocation + "&StartDate=" + StartDate + "&EndDate=" + EndDate;
            return await _httpClient.GetJsonAsync<List<CarViewModel>>(URL + par);
        }

        public async Task<CarViewModel> SelectById(int? Id)
        {
            return await _httpClient.GetJsonAsync<CarViewModel>($"{URL}/{Id}");
        }

        public async Task Update(CarViewModel entity, int? Id)
        {
            var response = await _httpClient.PutAsync($"{URL}/{Id}", new StringContent(JsonSerializer.Serialize(entity), Encoding.UTF8, "application/json"));

            if (response.IsSuccessStatusCode)
            {
                IsUpdated = true;
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.Conflict)
            {
                IsValid = false;
            }
            else
            {
                IsUpdated = false;
            }
        }
    }
}
