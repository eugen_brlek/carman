﻿using CarMan.Client.Helpers;
using CarMan.Client.Services.Interfaces;
using CarMan.Domain.ViewModels;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CarMan.Client.Services
{
    public class DashboardServices : IDashboardServices
    {
        private readonly HttpClient _httpClient;
        private const string URL1 = ApiUrls.Dashboard;
        private const string URL2 = ApiUrls.DashboardCar;

        public DashboardServices(
            HttpClient httpClient)
        {
            this._httpClient = httpClient;
        }

        public async Task<DashStatNumViewModel> GetSumStat()
        {
            return await _httpClient.GetJsonAsync<DashStatNumViewModel>(URL1);
        }

        public async Task<List<DashStatCarViewModel>> GetCarStat()
        {
            return await _httpClient.GetJsonAsync<List<DashStatCarViewModel>>(URL2);
        }
    }
}
