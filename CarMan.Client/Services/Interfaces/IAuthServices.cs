﻿using CarMan.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Client.Services.Interfaces
{
    public interface IAuthServices
    {
        Task Login(LoginViewModel entity);
    }
}
