﻿using CarMan.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Client.Services.Interfaces
{
    public interface ICarTypeServices
    {
        Task<List<CarTypeViewModel>> SelectAll();
        Task<CarTypeViewModel> SelectById(int? Id);
        Task Create(CarTypeViewModel entity);
        Task Update(CarTypeViewModel entity, int? Id);
        Task Delete(int? Id);
    }
}
