﻿using CarMan.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Client.Services.Interfaces
{
    public interface IEmployeeServices
    {
        Task<List<EmployeeViewModel>> SelectAll();
        Task<List<EmployeeViewModel>> SelectByName(string empName);
        Task<EmployeeViewModel> SelectById(int? Id);
        Task Create(EmployeeViewModel entity);
        Task Update(EmployeeViewModel entity, int? Id);
        Task Delete(int? Id);
    }
}
