﻿using CarMan.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Client.Services.Interfaces
{
    public interface ICarServices
    {
        Task<List<CarViewModel>> SelectAll();
        Task<List<CarViewModel>> SelectAvailable(string StartLocation, string EndLocation, DateTime? StartDate, DateTime? EndDate);
        Task<CarViewModel> SelectById(int? Id);
        Task Create(CarViewModel entity);
        Task Update(CarViewModel entity, int? Id);
        Task Delete(int? Id);
    }
}
