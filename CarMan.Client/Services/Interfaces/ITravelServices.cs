﻿using CarMan.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMan.Client.Services.Interfaces
{
    public interface ITravelServices
    {
        Task<List<TravelViewModel>> SelectAll();
        Task<List<TravelViewModel>> SelectAllSearched(DateTime? StartDate, DateTime? EndDate);
        Task<TravelViewModel> SelectById(int? Id);
        Task Create(TravelViewModel entity);
        Task Update(TravelViewModel entity, int? Id);
        Task Delete(int? Id);
    }
}
